#pragma once

#include <memory>
#include <d3d11.h>
#include <directxmath.h>
using namespace DirectX;

#include "ishader.h"
#include "imodelbuffer.h"

class ModelClass
{
public:
	ModelClass();
	ModelClass(const ModelClass&) = delete;
	~ModelClass();

	bool Initialize(ID3D11Device* device, std::unique_ptr<IShader> shader);
	void Shutdown();
	bool Render(ID3D11DeviceContext*, const IShader::MatrixBufferType& matrixBuf);

private:
	void RenderBuffer(ID3D11DeviceContext*);

private:
	std::unique_ptr<IModelBuffer> m_modelBuffer;
	std::unique_ptr<IShader> m_shader;
};