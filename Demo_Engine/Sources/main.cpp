#include "systemclass.h"

#include <memory>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	{
		auto system = std::make_unique<SystemClass>();
		bool result;


		// Check the system object.
		if (!system)
		{
			return 0;
		}

		// Initialize and run the system object.
		result = system->Initialize();
		if (result)
		{
			system->Run();
		}

		// Shutdown and release the system object.
		system->Shutdown();
	}

	return 0;
}