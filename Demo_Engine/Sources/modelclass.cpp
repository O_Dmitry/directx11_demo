#include "modelclass.h"

ModelClass::ModelClass()
{
}


ModelClass::~ModelClass()
{
	Shutdown();
}

bool ModelClass::Initialize(ID3D11Device* device, std::unique_ptr<IShader> shader)
{
	bool result;

	// Initialize shader
	m_shader = std::move(shader);

	// Initialize the vertex and index buffers.
	m_modelBuffer = m_shader->getModelBuffer();
	result = m_modelBuffer->initialize(device);
	if (!result)
	{
		return false;
	}

	return true;
}

void ModelClass::Shutdown()
{
	// Shutdown the vertex and index buffers.
	if (m_modelBuffer)
	{
		m_modelBuffer->clear();
		m_modelBuffer.reset();
	}
	
	// Release the shader object.
	if (m_shader)
	{
		m_shader->Shutdown();
		m_shader.reset();
	}

	return;
}

bool ModelClass::Render(ID3D11DeviceContext* deviceContext, const IShader::MatrixBufferType& matrixBuf)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffer(deviceContext);

	// Render the model using the texture shader.
	bool result = m_shader->Render(deviceContext, m_modelBuffer->getIndexCount(), matrixBuf);
	if (!result)
	{
		return false;
	}

	return true;
}


void ModelClass::RenderBuffer(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = m_modelBuffer->getVertexSize();
	offset = 0;
	auto vertexBuffer = m_modelBuffer->getVertexBuf();

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_modelBuffer->getIndexBuf(), DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}