#pragma once

#include <d3d11.h>
#include <directxmath.h>
#include <fstream>
#include <memory>

#include "imodelbuffer.h"

class IShader {
public:
	struct MatrixBufferType
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX view;
		DirectX::XMMATRIX projection;
	};

public:
	IShader();
	virtual ~IShader();
	
	virtual bool Initialize(ID3D11Device* device, HWND hwnd, ID3D11DeviceContext* deviceContext) = 0;
	virtual void Shutdown() = 0;
	virtual bool Render(ID3D11DeviceContext*, int, const MatrixBufferType& matrixBuf);	

	virtual std::unique_ptr<IModelBuffer> getModelBuffer() = 0;

protected:
	bool SetShaderParameters(ID3D11DeviceContext*, const MatrixBufferType& matrixBuf);
	void RenderShader(ID3D11DeviceContext*, int);

	void ShutdownShader();

	static void OutputShaderErrorMessage(ID3D10Blob*, HWND, LPCWSTR);

protected:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
};