#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <string>
#include <vector>
#include <memory>
using namespace DirectX;

#include "ishader.h"
#include "textureclass.h"

class TextureShaderClass: public IShader
{
public:
	TextureShaderClass() = delete;
	TextureShaderClass(const char* pathToTexture);
	TextureShaderClass(const TextureShaderClass&) = delete;
	virtual ~TextureShaderClass();

	virtual bool Initialize(ID3D11Device*, HWND, ID3D11DeviceContext*) override;
	virtual void Shutdown() override;
	virtual bool Render(ID3D11DeviceContext*, int, const MatrixBufferType& matrixBuf) override;

	virtual std::unique_ptr<IModelBuffer> getModelBuffer() override;

private:
	bool InitializeShader(ID3D11Device*, HWND, LPCWSTR, LPCWSTR);	

	bool LoadTexture(ID3D11Device*, ID3D11DeviceContext*, const std::string& pathToTexture);
	void ReleaseTexture();

private:
	ID3D11SamplerState* m_sampleState;
	TextureClass* m_Texture;
	std::string m_pathToTexture;
};

class TextureModelBuffer : public IModelBuffer {
public:
	virtual bool initialize(ID3D11Device* device) override;
private:
	struct VertexType {
		XMFLOAT3 position;
		XMFLOAT2 texture;
	};
};