#pragma once

#include <d3d11.h>

class IModelBuffer {
public:
	IModelBuffer();
	IModelBuffer(const IModelBuffer&) = delete;
	~IModelBuffer();

	virtual bool initialize(ID3D11Device* device) = 0;
	void clear();
	ID3D11Buffer* getVertexBuf() {
		return m_vertexBuffer;
	}
	ID3D11Buffer* getIndexBuf() {
		return m_indexBuffer;
	}
	int getVertexCount() {
		return m_vertexCount;
	}
	int getIndexCount() {
		return m_indexCount;
	}
	int getVertexSize() {
		return m_vertexSize;
	}

protected:
	bool init(ID3D11Device* device, const void* vertices, const void* indices);
	
protected:
	ID3D11Buffer* m_vertexBuffer, * m_indexBuffer;
	int m_vertexCount, m_indexCount;
	int m_vertexSize, m_indexSize;
};
