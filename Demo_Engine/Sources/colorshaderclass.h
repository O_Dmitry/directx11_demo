#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <memory>
using namespace DirectX;

#include "ishader.h"
#include "imodelbuffer.h"

class ColorShaderClass: public IShader
{
public:
	ColorShaderClass();
	ColorShaderClass(const ColorShaderClass&) = delete;
	virtual ~ColorShaderClass();

	virtual bool Initialize(ID3D11Device* device, HWND hwnd, ID3D11DeviceContext* deviceContext) override;
	virtual void Shutdown() override;

	virtual std::unique_ptr<IModelBuffer> getModelBuffer() override;

private:
	bool InitializeShader(ID3D11Device*, HWND, LPCWSTR, LPCWSTR);
};

class ColorModelBuffer : public IModelBuffer {
public:
	virtual bool initialize(ID3D11Device* device) override;

private:
	struct VertexType {
		XMFLOAT3 position;
		XMFLOAT4 color;
	};
};
