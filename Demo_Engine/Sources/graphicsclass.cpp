#include "graphicsclass.h"


GraphicsClass::GraphicsClass()
{
}


GraphicsClass::~GraphicsClass()
{
	Shutdown();
}


bool GraphicsClass::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
	bool result;


	// Create the Direct3D object.
	m_Direct3D = std::make_unique<D3DClass>();
	if (!m_Direct3D)
	{
		return false;
	}

	// Initialize the Direct3D object.
	result = m_Direct3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	// Create the camera object.
	m_Camera = std::make_unique<CameraClass>();
	if (!m_Camera)
	{
		return false;
	}

	// Set the initial position of the camera.
	m_Camera->SetPosition(0.0f, 0.0f, -4.0f);


	// Create the model object.
	m_Model = std::make_unique<ModelClass>();
	if (!m_Model)
	{
		return false;
	}

	
	// Create the shader object.
	auto shader = 
#ifdef TEXTURE_DEMO // texture shader
		std::make_unique<TextureShaderClass>("../Demo_Engine/Textures/stone-wall-texture.tga");
#elif defined(COLOR_DEMO) // color shader
		std::make_unique<ColorShaderClass>();
#endif
	if (!shader)
	{
		return false;
	}

	// Initialize the texture shader object.
	result = shader->Initialize(m_Direct3D->GetDevice(), hwnd, m_Direct3D->GetDeviceContext());
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the shader object.", L"Error", MB_OK);
		return false;
	}

	// Initialize the model object.	
	result = m_Model->Initialize(m_Direct3D->GetDevice(), std::move(shader));
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
}

	return true;
}


void GraphicsClass::Shutdown()
{
	// Release the model object.
	if (m_Model)
	{
		m_Model->Shutdown();
		m_Model.reset();
	}

	// Release the camera object.
	if (m_Camera)
	{
		m_Camera.reset();
	}

	// Release the Direct3D object.
	if (m_Direct3D)
	{
		m_Direct3D->Shutdown();
		m_Direct3D.reset();
	}

	return;
}

bool GraphicsClass::Frame(const InputClass* input)
{
	bool result;

	XMFLOAT3 pos{ m_Camera->GetPosition() };
	XMFLOAT3 rot{ m_Camera->GetRotation() };

	const float alpha = 1.0f; // degree
	const float shift = 0.05f;

	auto rotateY = [&pos, &rot](float alpha, XMFLOAT3 currentPos, XMFLOAT3 currentRot) {
		float alphaR = alpha * 3.14159265359f / 180.0f; // radians

		pos.x = currentPos.x * cos(alphaR) - currentPos.z * sin(alphaR);
		pos.z = currentPos.x * sin(alphaR) + currentPos.z * cos(alphaR);

		rot.y = currentRot.y - alpha;
	};
	auto rotateX = [&pos, &rot](float alpha, XMFLOAT3 currentPos, XMFLOAT3 currentRot) {
		float alphaR = alpha * 3.14159265359f / 180.0f; // radians

		pos.y = currentPos.y * cos(alphaR) - currentPos.z * sin(alphaR);
		pos.z = currentPos.y * sin(alphaR) + currentPos.z * cos(alphaR);

		rot.x = currentRot.x + alpha;
	};

	if (input->IsKeyDown(VK_SHIFT)) {
		if (!(input->IsKeyDown('A') && input->IsKeyDown('D'))) {
			if (input->IsKeyDown('A'))
				pos.x -= shift;
			if (input->IsKeyDown('D'))
				pos.x += shift;
		}
		if (!(input->IsKeyDown('W') && input->IsKeyDown('S'))) {
			if (input->IsKeyDown('W'))
				pos.y += shift;
			if (input->IsKeyDown('S'))
				pos.y -= shift;
		}
	}
	else {
		if (!(input->IsKeyDown('A') && input->IsKeyDown('D'))) {
			if (input->IsKeyDown('A'))
				rotateY(-alpha, pos, rot);
			else if (input->IsKeyDown('D'))
				rotateY(alpha, pos, rot);
		}
		if (!(input->IsKeyDown('W') && input->IsKeyDown('S'))) {
			if (input->IsKeyDown('W'))
				rotateX(alpha, pos, rot);
			else if (input->IsKeyDown('S'))
				rotateX(-alpha, pos, rot);
		}
	}

	// Set new camera position
	m_Camera->SetPosition(pos.x, pos.y, pos.z);
	m_Camera->SetRotation(rot.x, rot.y, rot.z);

	// Render the graphics scene.
	result = Render();
	if (!result)
	{
		return false;
	}

	return true;
}


bool GraphicsClass::Render()
{
	IShader::MatrixBufferType matrixBuf;
	bool result;


	// Clear the buffers to begin the scene.
	m_Direct3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Render();

	// Get the world, view, and projection matrices from the camera and d3d objects.
	m_Direct3D->GetWorldMatrix(matrixBuf.world);
	m_Camera->GetViewMatrix(matrixBuf.view);
	m_Direct3D->GetProjectionMatrix(matrixBuf.projection);

	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	result = m_Model->Render(m_Direct3D->GetDeviceContext(), matrixBuf);
	if (!result)
	{
		return false;
	}

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();

	return true;
}